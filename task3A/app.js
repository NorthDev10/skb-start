var express = require('express');

var app = express();
var fetch = require('node-fetch');

let pc = {"board":{"vendor":"IBM","model":"IBM-PC S-100","cpu":{"model":"80286","hz":12000},"image":"http://www.s100computers.com/My%20System%20Pages/80286%20Board/Picture%20of%2080286%20V2%20BoardJPG.jpg","video":"http://www.s100computers.com/My%20System%20Pages/80286%20Board/80286-Demo3.mp4"},"ram":{"vendor":"CTS","volume":1048576,"pins":30},"os":"MS-DOS 1.25","floppy":0,"hdd":[{"vendor":"Samsung","size":33554432,"volume":"C:"},{"vendor":"Maxtor","size":16777216,"volume":"D:"},{"vendor":"Maxtor","size":8388608,"volume":"C:"}],"monitor":null,"length":42,"height":21,"width":54};


app.get('/*', function(req, res) {
  res.send(searchInfo(req, res));
});

function searchInfo(req, res) {
  var body;
  if(req.url == '/') {
    body = pc;
  } else {
    var path = req.url.split('/');
    if(path[1] == 'volumes') {
      var volumes = {};
      for(var i = 0; i < pc.hdd.length; i++) {
        volumes[pc.hdd[i]['volume']] = (parseInt(volumes[pc.hdd[i]['volume']] || 0) + pc.hdd[i]['size']) + 'B';
      }
      body = volumes;
    } else {
      var tPc = pc;
      for(var i = 1; i < path.length; i++) {
        tPc = tPc[path[i]];
        if(typeof(tPc) == 'undefined') {
          res.status(404);
          return 'Not Found';
        }
        if(tPc == null) {
          return 'null';
        }
      }
      body = tPc;
    }
  }
  res.setHeader('Content-Type', 'application/json');
  return JSON.stringify(body);
}

app.listen(3001, 'localhost');