var express = require('express');

var app = express();

app.get('/', function(req, res) {
  var a = parseFloat(req.param('a'));
  var b = parseFloat(req.param('b'));
  if(isNaN(a)) a = 0;
  if(isNaN(b)) b = 0;
  var result = a + b;
  res.send(result.toString());
});

app.listen(3001, 'localhost');