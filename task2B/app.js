var express = require('express');

var app = express();

app.get('/', function(req, res) {
  var fullname = req.param('fullname');
  var arrFullname = fullname.split(' ');
  switch(arrFullname.length) {
    case 1:
      fullname = arrFullname[0];
    break;
    case 2:
      fullname = arrFullname[1]+' '+arrFullname[0][0]+'.';
    break;
    case 3:
      fullname = arrFullname[2]
        +' '+arrFullname[0][0]
        +'. '
        +arrFullname[1][0]
        +'.';
    break;
    default:
      fullname = 'Invalid fullname';
  }
  res.send(fullname);
});

app.listen(3001, 'localhost');