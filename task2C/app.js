var express = require('express');

var app = express();

app.get('/', function(req, res) {
  var username = req.param('username');
  if(username) {
    var result = username.match(/[\w][\/]([\w]+)|^([\w]+)$/i);
    if(result) {
      username = '@' + (result[1] || result[2]);
    } else {
      username = 'Invalid username';
    }
  } else {
    username = 'Invalid username';
  }
  res.send(username);
});

app.listen(3001, 'localhost');